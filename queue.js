let collection = [];


// Write the queue functions below.


// 1. Output all the elements of the queue
	
	const print = () => collection;


// 2. Adds element to the rear of the queue

	const enqueue = (element) => {

    if (collection.length === 0) {
        collection[0] = element;

    } else {
        collection[collection.length] = element;
    }

    return collection;
};


// 3. Removes element from the front of the queue

	const dequeue = () => {

    let newArr = [];

    for(let i = 0; i < collection.length - 1; i++){
    for(let i = 0; i < collection.length - 1; i++){
            newArr[i] = collection[i];
            }
    }

    collection.length = 0;

    for(i = 0; i < newArr.length; i++){
        collection[i] = newArr[i];
    }
    
    return collection;
};


// 4. Show element at the front

const front = () => collection[0];

// 5. Show the total number of elements

const size = () => collection.length;

// 6. Outputs a Boolean value describing whether queue is empty or not

const isEmpty = (collection) => collection === [];


module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};